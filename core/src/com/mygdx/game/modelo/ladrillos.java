package com.mygdx.game.modelo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ladrillos {
    public ArrayList<ladrillo> mapa;

    public ladrillos(File map, Graphics.DisplayMode dm){
        StringBuilder text = new StringBuilder();
        mapa = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(map));
            String line;
            int j = 0;
            while ((line = br.readLine()) != null) {
                int i = 0;
                while(i<line.length()){
                    if(line.charAt(i) != ' '){
                        Texture t = new Texture("ladrillos.jpg");
                        ladrillo aux = new ladrillo(t,i*dm.width/15, dm.height*0.8f - (j+1)*dm.height/20, dm.width/15, dm.height/20);
                        pintar(line.charAt(i), aux, t);
                    }
                    i++;
                }
                j++;
            }
            br.close();
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }

    }

    public void pintar(char val, ladrillo aux, Texture t){
        switch (val){
            case 'r':
                aux.personaje.setRegion(new TextureRegion(t,0,0,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 1;
                mapa.add(aux);
                break;
            case 'o':
                aux.personaje.setRegion(new TextureRegion(t,t.getWidth()/3,0,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 2;
                mapa.add(aux);
                break;
            case 'y':
                aux.personaje.setRegion(new TextureRegion(t,t.getWidth()*2/3,0,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 3;
                mapa.add(aux);
                break;
            case 'g':
                aux.personaje.setRegion(new TextureRegion(t,0,t.getHeight()/3,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 4;
                mapa.add(aux);
                break;
            case 'a':
                aux.personaje.setRegion(new TextureRegion(t,t.getWidth()/3,t.getHeight()/3,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 5;
                mapa.add(aux);
                break;
            case 'c':
                aux.personaje.setRegion(new TextureRegion(t,t.getWidth()*2/3,t.getHeight()/3,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 6;
                mapa.add(aux);
                break;
            case 'b':
                aux.personaje.setRegion(new TextureRegion(t,0,t.getHeight()*2/3,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 7;
                mapa.add(aux);
                break;
            case 'm':
                aux.personaje.setRegion(new TextureRegion(t,t.getWidth()/3,t.getHeight()*2/3,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 8;
                mapa.add(aux);
                break;
            case 'v':
                aux.personaje.setRegion(new TextureRegion(t,t.getWidth()*2/3,t.getHeight()*2/3,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 9;
                mapa.add(aux);
                break;
        }
    }

    public void valor(int valor, ladrillo aux, Texture t){
        switch (valor){
            case 1:
                pintar('r', aux, t);
                break;
            case 2:
                pintar('o', aux, t);
                break;
            case 3:
                pintar('y', aux, t);
                break;
            case 4:
                pintar('g', aux, t);
                break;
            case 5:
                pintar('a', aux, t);
                break;
            case 6:
                pintar('c', aux, t);
                break;
            case 7:
                pintar('b', aux, t);
                break;
            case 8:
                pintar('m', aux, t);
                break;
            case 9:
                pintar('v', aux, t);
                break;
        }
    }
}
