package com.mygdx.game.pantalla;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.game.controlador.gameController;
import com.mygdx.game.modelo.Personaje;
import com.mygdx.game.modelo.ladrillos;
import com.mygdx.game.modelo.pelota;
import com.mygdx.game.modelo.plataforma;

import java.io.File;
import java.util.LinkedList;

public class PantallaJuego extends Base{
    SpriteBatch batch;
    float tiempo;
    gameController control;
    Texture tgoopy2;
    pelota goopy;
    ladrillos pared;
    plataforma platform;
    int index;
    Object[] pers;
    Graphics.DisplayMode dm;
    float repeticiones;
    Music musica;

    public PantallaJuego(final ProyectoGrafica game, int nivel, Graphics.DisplayMode dm1){
        super(game);
        dm = dm1;
        index = 0;
        repeticiones = 0f;
        batch = game.getSB();
        tiempo = 0f;
        tgoopy2 = new Texture("goopy_quieto.png");
        platform = new plataforma(new Texture("plataforma.png"),dm.width/2 - dm.width*0.15f/2, dm.height*0.1f, dm.width*0.15f, dm.height*0.1f);
        goopy = new pelota(tgoopy2, dm.width/2 - dm.width * 0.0125f/2, dm.height*0.1f + dm.height*0.025f,dm.width * 0.025f, dm.width * 0.025f);
        pared = new ladrillos(new File(String.valueOf(nivel%10)+".txt"),dm);
        control = new gameController(nivel);
        pers = new Object[3];
        pers[0] = platform;
        pers[1] = goopy;
        pers[2] = pared;
        musica = Gdx.audio.newMusic(Gdx.files.internal("musica.mp3"));
        musica.setLooping(true);
        musica.setVolume(0.1f);
        musica.play();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(control);
    }

    @Override
    public void render(float delta) {
        tiempo += Gdx.graphics.getDeltaTime();
        ScreenUtils.clear(0.4f, 0, 0.6f, 1);
        batch.begin();
        pers = control.cambios(pers, dm);
        platform = (plataforma) pers[0];
        goopy = (pelota) pers[1];
        pared = (ladrillos) pers[2];
        goopy.render(batch, tiempo);
        platform.render(batch, tiempo);
        while(index < pared.mapa.size()){
            if(pared.mapa.get(index).activo)
                pared.mapa.get(index).render(batch, tiempo);
            else{
                while(repeticiones<= 2.5f &&pared.mapa.get(index).animado){
                    pared.mapa.get(index).render(batch, tiempo);
                    repeticiones += Gdx.graphics.getDeltaTime();
                }
                pared.mapa.get(index).animado = false;
                repeticiones = 0f;
            }
            index++;
        }
        index = 0;
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
        batch.dispose();
    }
}
